import React from "react";
import { useSelector } from "react-redux";
import { State, User } from "../reducers";
import { Button } from "antd";
import history from "../history";
import UserCard from "./UserCard";

const UserList = () => {
  const users: User[] = useSelector((state: State) => state.users);

  return (
    <div className={"user__list"}>
      <Button
        type={"primary"}
        onClick={() => history.push("/new")}
        data-cy={"button"}
      >
        Add User
      </Button>
      {users.map((user) => {
        return (
          <UserCard key={user.id} user={user} data-cy={`user-${user.id}`} />
        );
      })}
    </div>
  );
};

export default UserList;
