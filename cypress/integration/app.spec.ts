import * as faker from "faker";

const createUser = (name: string, email: string, phone: string) => {
  cy.get("~button").click();

  cy.location("pathname").should("equal", "/new");

  cy.get("~full-name").type(name).should("have.value", name);
  cy.get("~email").type(email).should("have.value", email);
  cy.get("~phone").type(phone).should("have.value", phone);
  cy.get("~submit").click();

  cy.location("pathname").should("equal", "/");

  cy.contains(name);
  cy.contains(email);
  cy.contains(phone);
};

const createMultipleUsers = (count: number) => {
  Cypress._.range(count).forEach(() => {
    createUser(
      faker.name.findName(),
      faker.internet.email(),
      faker.phone.phoneNumber()
    );
  });
};

describe("App Test", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should create a new user", function () {
    createUser("John Snow", "john@snow.com", "53551721");
  });

  it("should show errors for wrong input", function () {
    cy.get("~button").click();

    cy.get("~email").type("aaaaa").blur();

    cy.contains("This input is not valid E-mail!");
  });

  it("should edit a created user", function () {
    const name = "John Snow";
    const email = "john@snow.com";
    const phone = "53551721";
    createUser(name, email, phone);
    cy.get("~edit-button-1").click();

    cy.get("~full-name")
      .should("have.value", name)
      .type("{backspace}{backspace}{backspace}{backspace}Crow")
      .should("have.value", "John Crow");

    cy.get("~email")
      .should("have.value", email)
      .type("{backspace}{backspace}{backspace}net")
      .should("have.value", "john@snow.net");

    cy.get("~phone")
      .should("have.value", phone)
      .type("{backspace}2")
      .should("have.value", "53551722");

    cy.get("~submit").click();

    cy.contains("John Crow");
    cy.contains("john@snow.net");
    cy.contains("53551722");
  });

  it("should add multiple users", function () {
    const userCount = 10;
    createMultipleUsers(userCount);

    cy.get("[data-test=user]").should("have.length", userCount);
  });

  it("should persist multiple users across reloads", function () {
    const userCount = 10;
    createMultipleUsers(userCount);

    cy.get("[data-test=user]").should("have.length", userCount);

    cy.reload();

    cy.get("[data-test=user]").should("have.length", userCount);
  });
});
